﻿using System;

namespace exercise_14
{
    class Program
    {
        static void Main(string[] args)
        {
         //Start the program with Clear();
         Console.Clear();
         Console.WriteLine("what month are you born");

         var month = int.Parse(Console.ReadLine());
         
         
         //End the program with blank line and instructions
         Console.ResetColor();
         Console.WriteLine();
         Console.WriteLine("Press <Enter> to quit the program");
         Console.ReadKey();
        }
    }
}
